<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class MessagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendingMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'receiver_id' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $to_user = User::where('id', $request->receiver_id)->first();

        $message = Message::create([
            'sender_id' => $user->id,
            'receiver_id' => $request->receiver_id,
            'message' => $request->message,
        ]);


        //notification
        $title = 'فرصة';
        $content = 'رسالة جديدة';
        $fcm_message = [
            'message_id' => (int)$message->id,
            'sender_id' => (int)$user->id,
            "message" => $message->message ?: null,
            'since' => $message->created_at->diffForHumans(),
            'created_at' => $message->created_at,
            'type' => 'message',
            "sender" => [
                'id' => $user->id,
                'name' => $message->sender->username ?: "",
                'photo' => $message->sender->photo ? \Helpers::base_url() . $message->sender->photo : "",
                'tokens' => $message->sender->tokens,
            ]
        ];

        Notification::send($to_user, new NewMessageNotification($message));
        \Helpers::fcm_notification($to_user->device_token, $content, $title, $fcm_message);


        return response()->json([], 204);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReceivedMessages()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $received_messages = Message::select('*')->
        from(DB::raw("
    ( 
      SELECT *,`sender_id` as `theuser`
      FROM `messages` WHERE `receiver_id` = $user->id
      union
      SELECT *,`receiver_id` as `theuser`
      FROM `messages` WHERE `sender_id` = $user->id
      order by `created_at` desc
    ) as `sub`"))->groupBy('theuser')->
        orderBy('created_at', 'desc')->
        get();
        $received_messages->transform(function ($i) use ($user) {
            if ($user->id == $i->sender_id) {
                $i->sender = User::select(['id', 'username', 'tokens', 'photo'])->where('id', $i->sender_id)->first();
                $i->receiver = User::select(['id', 'username', 'tokens', 'photo'])->where('id', $i->receiver_id)->first();
            } else {
                $i->sender = User::select(['id', 'username', 'tokens', 'photo'])->where('id', $i->receiver_id)->first();
                $i->receiver = User::select(['id', 'username', 'tokens', 'photo'])->where('id', $i->sender_id)->first();
            }

            return $i;
        });

        return response()->json(['messages' => $received_messages], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function chatDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
//            'receiver_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $to_user = User::where('id', $request->user_id)->first();

        $data = Message::select(['id', 'message', 'receiver_id', 'sender_id', 'created_at'])
            ->where(function ($query) use ($user, $to_user) {
                $query->where([['sender_id', $user->id], ['receiver_id', $to_user->id]]);
                $query->orWhere([['sender_id', $to_user->id], ['receiver_id', $user->id]]);
            })->orderBy('created_at', 'asc')->with(['sender' => function ($q) {
                $q->select(['id', 'username', 'tokens', 'photo'])->get();
            }]);
        $messages = $data->get();
        $messages->transform(function ($i) {
            $i->since = $i->created_at->diffForHumans();

            return $i;
        });

        //read messages
//        $data->update(['is_seen' => 1]);

        return response()->json([
            'messages' => $messages,
        ], 200);
    }

}
