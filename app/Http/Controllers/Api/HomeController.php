<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CityCollection;
use App\Http\Resources\CountryCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\Setting;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * @return mixed
     */
    public function getCountries()
    {
        $countries = Country::all();
        return response()->json(new CountryCollection($countries), 200);

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCities($id)
    {
        $cities = City::where('country_id', $id)->get();
        return response()->json(new CityCollection($cities), 200);
    }

    public function getSettings()
    {
        $settings = \App\Models\Setting::first();
        return response()->json(new Setting($settings), 200);

    }


    /**
     * @return mixed
     */
    public function getHome()
    {
        $cats = Category::where('parent_id', null)->get();
        if (\Helpers::getCurrentCity() == 0) {
            $country_id = \Helpers::getCurrentCountry();
            $cities = City::where('country_id', $country_id)->pluck('id');
            $latest_products = Product::whereIn('city_id', $cities)->latest()->take(10)->get();
            $most_viewed_products = Product::orderBy('views', 'DESC')->whereIn('city_id', $cities)->take(10)->get();
        } else {
            $city_id = \Helpers::getCurrentCity();
            $latest_products = Product::latest()->where('city_id', $city_id)->take(10)->get();
            $most_viewed_products = Product::orderBy('views', 'DESC')->where('city_id', $city_id)->take(10)->get();
        }

        return response()->json([
            'categories' => new CategoryCollection($cats),
            'latest_products' => new ProductCollection($latest_products),
            'most_viewed_products' => new ProductCollection($most_viewed_products),
        ], 200);
    }
}

