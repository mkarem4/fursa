<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\Message;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOption;
use App\Models\Slider;
use App\Notifications\NewMessageNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchFilter(Request $request)
    {
        $country_id = session('country_id');
        $products = Product::whereHas('user');
        if ($request->city_id) {
            $products = $products->where('city_id', $request->city_id);
        } else {
            $cities_ids = City::where('country_id', $country_id)->pluck('id')->toArray();
            $products = $products->whereIn('city_id', $cities_ids);
        }
        if ($request->category_id) {
            $ids = [];
            $category = Category::find($request->category_id);
            $children = Category::where('parent_id', $category->id)->get();
            foreach ($children as $child) {
                $temp = Category::where('parent_id', $child->id)->pluck('id')->toArray();
                array_push($ids, ...$temp);
            }

            $products = $products->whereIn('category_id', $ids);
        }
        if ($request->subcategory_id) {
            $ids = Category::where('parent_id', $request->subcategory_id)->pluck('id')->toArray();
            $products = $products->whereIn('category_id', $ids);
        }

        if ($request->type) {
            $products = $products->where('category_id', $request->type);
        }
        if (isset($request->options)) {
            $options = json_decode($request->options, true);
            foreach ($options as $key => $value) {
                if ($value['value'] != '') {
                    $option_id = $value['id'];
                    $products = $products->whereHas('ProductOption', function ($q) use ($value, $option_id) {
                        $q->where('option_id', $option_id)->where('option_value_id', $value['value']);
                    });
                }
            }
        }
        if ($request->price_start && !$request->price_end) {
            $products = $products->where('price', '>=', $request->price_start);
        }
        if (!$request->price_start && $request->price_end) {
            $products = $products->where('price', '<=', $request->price_end);
        }
        if ($request->price_start && $request->price_end) {
            $products = $products->whereBetween('price', array($request->price_start, $request->price_end));
        }


        $products = $products
            ->with(['ProductOption', 'ProductImage' => function ($q) {
                $q->select(['id', 'product_id', 'image']);
            }])->get();

        return response()->json($products);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $city_id = session('city_id');
        $country_id = session('country_id');

        $products = Product::where('title', 'LIKE', "%{$request->search}%");
        if ($city_id) {
            $products = $products->where('city_id', $city_id);
            $cities = City::where('country_id', $country_id)->get();
        } elseif (session()->has('country_id') && !session()->has('city_id')) {
            $cities = City::where('country_id', $country_id)->get();
            $cities_ids = City::where('country_id', $country_id)->pluck('id')->toArray();
            $products = $products->whereIn('city_id', $cities_ids);
        } else {
            $country = Country::where('name_ar', 'السعودية')->first();
            $cities = City::where('country_id', $country->id)->get();
        }

        $products = $products->get();
        $categories = Category::where('parent_id', null)->get();
        $sliders = Slider::where('location', 2)->get();
        return view('website.products.search', compact('products', 'cities', 'categories', 'sliders'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSubCategories($name)
    {
        $category = Category::where('name_ar', $name)->orWhere('name_en', $name)->first();
        $categories = Category::where('parent_id', null)->get();
        $current_lang = App::getLocale();
        return view('website.products.categories', compact('category', 'current_lang', 'categories'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProduct($id)
    {
        $product = Product::find($id);
        $product->update([
            'views' => $product->views + 1
        ]);
        $similar_products = Product::where('category_id', $product->category_id)->where('id', '!=', $product->id)->get()->take(4);
        $most_watched_products = Product::orderBy('views', 'desc')->where('id', '!=', $product->id)->get()->take(4);
        $categories = Category::where('parent_id', null)->get();

        return view('website.products.product_details', compact('product', 'similar_products', 'most_watched_products', 'categories'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProducts($id)
    {
        $category_id = $id;
        $ids = Category::where('parent_id', $id)->pluck('id')->toArray();

        $country_id = session('country_id');
        $city_id = session('city_id');
        $products = Product::whereIn('category_id', $ids);
        if ($city_id) {
            $cities = City::where('country_id', session('country_id'))->get();
            $products = $products->where('city_id', $city_id);

        } else {
            $cities = City::where('country_id', session('country_id'))->get();
            $cities_ids = City::where('country_id', $country_id)->pluck('id')->toArray();
            $products = $products->whereIn('city_id', $cities_ids);
        }
        if (!$country_id) {
            $country = Country::where('name_ar', 'السعودية')->first();
            $cities = City::where('country_id', $country->id)->get();
        }


        $products = $products->get();

        $categories = Category::where('parent_id', null)->get();
        $current_lang = App::getLocale();
        $sliders = Slider::where('location', 3)->get();
        return view('website.products.products', compact('products', 'categories', 'current_lang', 'sliders', 'cities', 'category_id'));
    }

    /**
     * @param
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function AddProduct()
    {
        $categories = Category::with('children')->where('parent_id', null)->get();
        $countries = Country::get();
        return view('website.products.add', compact('countries', 'categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function SaveProduct(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'phone' => 'required|string',
            'price' => 'required|string|max:6',
            'description' => 'sometimes|nullable|string',
            'type' => 'required',
            'city_id' => 'required',

            'options' => 'array',
        ]);
        $data = [
            'user_id' => auth()->id(),
            'title' => $request->title,
            'phone' => $request->phone,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->type,
            'city_id' => $request->city_id,
        ];
        $product = Product::create($data);
        $product_id = $product->id;

        $images = json_decode(request()->images);
        if ($images) {
            if (count($images)) {
                foreach ($images as $image) {
                    $save_image = new ProductImage();
                    $save_image->product_id = $product_id;
                    $save_image->image = $image;
                    $save_image->save();
                }
            }
        }

        if (request()->value) {
            foreach (request()->value as $key => $option) {
                if ($option) {
                    $dataa = new ProductOption();
                    $dataa->option_value_id = $option;
                    $dataa->product_id = $product_id;
                    $dataa->option_id = request()->option_id[$key];
                    $dataa->save();
                }
            }
        }

        if ($request->kilometer) {
            $new_option = Option::firstOrCreate([
                'name_ar' => "كيلوميترات",
                'name_en' => "Kilometers",
                'category_id' => $request->type,
                'icon' => '/uploads/gUH1ho4l5i.png'
            ]);

            $option_value = OptionValue::firstOrCreate([
                'option_id' => $new_option->id,
                'value' => $request->kilometer,
            ]);

            $dataa = new ProductOption();
            $dataa->option_value_id = $option_value->id;
            $dataa->product_id = $product_id;
            $dataa->option_id = $new_option->id;
            $dataa->save();
        }

        if ($request->space) {
            $new_option = Option::firstOrCreate([
                'name_ar' => "المساحة",
                'name_en' => "space",
                'category_id' => $request->type,
                'icon' => '/uploads/categories/cROpd1NNDZ.jpg'
            ]);

            $option_value = OptionValue::firstOrCreate([
                'option_id' => $new_option->id,
                'value' => $request->space,
            ]);

            $dataa = new ProductOption();
            $dataa->option_value_id = $option_value->id;
            $dataa->product_id = $product_id;
            $dataa->option_id = $new_option->id;
            $dataa->save();
        }
        session()->flash('success', trans('site.add_ad_success'));
        return redirect(url('/'));
    }

    public function uploadAdImage(Request $request)
    {
        $data = $this->validate(request(), [
            'file' => 'required|max:1000',
        ], [], [
            'file' => 'logo',
        ]);
        if (request()->hasFile('file')) {
            $imageName = Str::random(10) . '.' . request()->File('file')->extension();
            request()->File('file')->move(
                base_path() . '/public/uploads/products/', $imageName
            );
            $data['file'] = '/uploads/products/' . $imageName;
        }
        echo $data['file'];
    }

    /**
     * @param $product_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function lowPrice($product_id)
    {
        $product = Product::find($product_id);
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        $to_user = User::where('id', $product->user_id)->first();
        $message_text = " ابلغني عند انخفاض سعر اعلان " . $product->title;
        $message = Message::create([
            'sender_id' => $user->id,
            'receiver_id' => $product->user_id,
            'message' => $message_text,
        ]);


        //notification
        $title = 'فرصة';
        $content = 'رسالة جديدة';
        $fcm_message = [
            'message_id' => (int)$message->id,
            'sender_id' => (int)$user->id,
            "message" => $message_text,
            'since' => $message->created_at->diffForHumans(),
            'created_at' => $message->created_at,
            'type' => 'message',
            "sender" => [
                'id' => $user->id,
                'name' => $message->sender->username ?: "",
                'photo' => $message->sender->photo ? \Helpers::base_url() . $message->sender->photo : "",
                'tokens' => $message->sender->tokens,
            ]
        ];

        Notification::send($to_user, new NewMessageNotification($message));
        \Helpers::fcm_notification($to_user->device_token, $content, $title, $fcm_message);

        session()->flash('success', trans('site.send_contact_successfully'));
        return redirect(url('/'));
    }


}
