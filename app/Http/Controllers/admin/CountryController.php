<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::latest()->paginate(10);
        return view('admin.countries.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
//            'flag' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($request->hasFile('flag')) {
            $imageName = Str::random(10) . '.' . $request->file('flag')->extension();
            $request->file('flag')->move(
                base_path() . '/public/uploads/flags/', $imageName
            );
        }
        Country::create(
            [
                'flag' => $request->hasFile('flag') ? '/uploads/flags/' . $imageName : null,
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
            ]
        );
        return redirect('/webadmin/countries')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم اضافة الدولة بنجاح']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        return view('admin.countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
//            'flag' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        if ($request->hasFile('flag')) {
            $imageName = Str::random(10) . '.' . $request->file('flag')->extension();
            $request->file('flag')->move(
                base_path() . '/public/uploads/flags/', $imageName
            );
            Country::find($id)->update([
                'flag' => '/uploads/flags/' . $imageName,
            ]);
        }
        Country::find($id)->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);
        return redirect('/webadmin/countries')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم تعديل الدولة بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::destroy($id);
        return redirect('/webadmin/countries')->withFlashMessage(json_encode(['success' => true, 'msg' => 'تم حذف الدولة بنجاح']));
    }
}
