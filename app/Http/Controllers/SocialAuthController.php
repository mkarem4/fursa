<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Socialite;
class SocialAuthController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user);
            return redirect()->to('/');


    }
    function createUser($getInfo,$provider){
        $user = User::where('provider_id', $getInfo->id)->first();
        if (!$user)
        {
            $oldemail=User::where('email', $getInfo->email)->first();

            if ($oldemail) {
                return $user;
            }else
            {
                $user = User::create([
                    'username' => $getInfo->name,
                    'active'     =>1,
                    'email' => $getInfo->email,
                    'provider' => $provider,
                    'provider_id' => $getInfo->id
                ]);
            }
        }
        return $user;
    }

}
