<?php

header("Content-Type: text/html; charset=utf-8");

//use App\Models\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

//use FCM;

class Helpers
{

    public static function base_url()
    {
        return URL::to('/');
    }


    public static function the_image($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/700x300.png';
        }
    }

    public static function the_image_sm($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/70x70.png';
        }
    }

    public static function category_image($one)
    {
        if ($one) {
            return url($one);
        } else {
            return 'https://via.placeholder.com/70x70.png';
        }
    }

    public static function failFindId()
    {
        if (request()->header('Accept-Language') == 'ar') {
            $message = 'لا يوجد نتائج ';
        } else {
            $message = 'No results for this id';
        }
        return $message;
    }


    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param $unit
     * @return float|int
     */


    /**
     * @return string
     */
    public static function generateRandomString()
    {
        return Str::random(255);

    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function CheckAuthorizedRequest()
    {
        return \request()->header('Access-Token');
    }


    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getLang()
    {
        return \request()->header('Accept-Language');
    }


    /**
     * @return array|string|null
     */
    public static function getCurrentCountry()
    {
        return \request()->header('country_id');
    }


    /**
     * @return array|string|null
     */
    public static function getCurrentCity()
    {
        return \request()->header('city_id');
    }

    /**
     * @return mixed
     */
    public static function getLoggedUser()
    {
        $user = User:: where('tokens', Helpers::CheckAuthorizedRequest())->first();
        if ($user)
            return $user;
        else
            return 'No results';
    }

    /**
     * @param $user
     * @return bool
     */
    public static function updateFCMToken($user)
    {
        $fcm_token = \request()->header('fcm-token');
        $user->update([
            'device_token' => $fcm_token ? $fcm_token : null
        ]);
        return true;
    }


    /**
     * @param $user
     * @return bool
     */

    public static function AdsLocation()
    {
        return [
            '1' => 'اعلي الصفحه',
            '2' => 'متوسط الصفحه',
            '3' => 'اسفل الصفحه',
            '4' => 'يمين',
            '5' => 'يسار',

        ];
    }

    public static function ReturnAds($id)
    {
        $ad = \App\Models\Slider::where('location', '=', $id)->first();
        return $ad;
    }


    /**
     * @param $token
     * @param $message
     * @return bool
     */
    public static function fcm_notification($token, $content, $title, $message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'notification' => $message,
            'sound' => true,
            'title' => $title,
            'body' => $content,
            'priority' => 'high',
        ];

        $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAAmHoWRrM:APA91bFvdvlaxf_VfivtUhfl2chpPJqwA7ZLRhx_oDpx_94twKHx7J30OnQLVOqDuCT9PeeAhHRLOAI2gczWb6-bxIlSK8cJoc01EJWw9msn66AjEROvY4zXIZKJhSCHHQraWMv5OZj5',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return true;
    }

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public static function getCurrency()
    {
        $country = session('country_id');
        if ($country == 1)
            return trans('site.sar');
        elseif ($country == 2)
            return trans('site.eg');
        elseif ($country == 4)
            return trans('site.ead');
        elseif ($country == 5)
            return trans('site.kwd');
        elseif ($country == 6)
            return trans('site.omr');
        elseif ($country == 7)
            return trans('site.qar');
        elseif ($country == 8)
            return trans('site.bhd');
        else
            return trans('site.sar');
    }


    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public static function getCurrencyOfCountry()
    {
        $country = self::getCurrentCountry();
        if ($country == 1)
            return trans('site.sar');
        elseif ($country == 2)
            return trans('site.eg');
        elseif ($country == 4)
            return trans('site.ead');
        elseif ($country == 5)
            return trans('site.kwd');
        elseif ($country == 6)
            return trans('site.omr');
        elseif ($country == 7)
            return trans('site.qar');
        elseif ($country == 8)
            return trans('site.bhd');
        else
            return trans('site.sar');
    }

}
