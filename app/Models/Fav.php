<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Fav extends Model
{
    protected $table = 'favs';
    protected $guarded = [];

    public $timestamps = true;


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
