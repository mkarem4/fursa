@extends('admin.layouts.app')

@section('title')
    تعديل بنر اعلاني
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/sliders')}}" class="m-menu__link">
            <span class="m-menu__link-text">الاعلانات </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">تعديل اعلان </span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تعديل اعلان
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($slider,['route' => ['ads.update' , $slider->id],'method'=> 'PATCH','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label for="city_id" class="col-lg-2 col-form-label">الموقع: </label>
                <div class="col-lg-10{{ $errors->has('location') ? ' has-danger' : '' }}">
                  <input  class="form-control  m-input" readonly value="{{ Helpers::AdsLocation()[$slider->location]}}">
                </div>
            </div>


            <div class="form-group m-form__group row">
                <label for="type" class="col-lg-2 col-form-label">النوع : </label>
                <div class="col-lg-10{{ $errors->has('type') ? ' has-danger' : '' }}">
                    <select name="type" id="type" onchange="java_script_:show(this.options[this.selectedIndex].value)"  class="form-control  m-input" required>
                        <option value="">قم باختيار نوع الاعلان </option>
                        <option value="1" {{ $slider->type==1?'selected':''}}  >صوره</option>
                        <option value="2" {{ $slider->type==2?'selected':''}}  >كود مضمن</option>
                    </select>
                    @if ($errors->has('type'))
                        <span class="form-control-feedback" role="alert">
                        <strong>{{ $errors->first('type') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label for="status" class="col-lg-2 col-form-label">الحاله : </label>
                <div class="col-lg-10{{ $errors->has('type') ? ' has-danger' : '' }}">
                    <select name="status" id="status"  class="form-control  m-input" required>
                        <option value="">قم باختيار حاله الاعلان </option>
                        <option value="1" {{ $slider->status==1?'selected':''}}  >مفعل</option>
                        <option value="2" {{ $slider->status==2?'selected':''}}  >غير مفعل</option>
                    </select>
                    @if ($errors->has('status'))
                        <span class="form-control-feedback" role="alert">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group row" style="display:@if($slider->type==1) flex @else none @endif"  id="image_div">
                <label class="col-lg-2 col-form-label">الصورة: </label>
                <div class="col-lg-10{{ $errors->has('path') ? ' has-danger' : '' }}">
                    <input type="file" name="path"   class="form-control uploadinput">
                    @if ($errors->has('path'))
                        <span class="form-control-feedback" role="alert">
                            <strong>{{ $errors->first('path') }}</strong>
                        </span>
                    @endif
                </div>
                @if(isset($slider) && $slider->path)
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">
                            <img data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                                 alt="First slide [800x4a00]"
                                 src="{{$slider->path}}"
                                 style="height: 150px; width: 150px"
                                 data-holder-rendered="true">
                        </div>

                    </div>
                @endif
            </div>
            <div class="form-group m-form__group row" style="display: @if($slider->type==1) none  @else flex @endif" id="code_div">
                <label for="code" class="col-lg-2 col-form-label">الكود المضمن : </label>
                <div class="col-lg-10{{ $errors->has('code') ? ' has-danger' : '' }}">
                   <textarea name="code" id="code"  rows="6" class="form-control  m-input">{{$slider->code}}</textarea>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success">تعديل</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">
        function show(val) {
            if (val == 1) {
                image_div.style.display='flex';
                code_div.style.display='none';

            }else if(val == 2){
                image_div.style.display='none';
                code_div.style.display='flex';
            }
            else{
                image_div.style.display='none';
                code_div.style.display='none';
            }
        }
    </script>
@endsection

