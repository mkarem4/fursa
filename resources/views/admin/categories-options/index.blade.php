@extends('admin.layouts.app')
@section('title')
    خصائص الاقسام
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">خصائص الاقسام</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        خصائص الاقسام
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <form method="get" action="{{url('/webadmin/categories-options')}}">
                <div class="row col-md-12">
                    <div class="form-group m-form__group col-md-11">
                        <label for="category_id" class="col-lg-2 col-form-label">اختيار  القسم </label>
                        <div class="col-lg-10">
                            <select name="category_id" id="category_id" class="form-control m-input--solid">
                                <option value="">قم باختيار القسم </option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if(isset($category_id) && $category->id == $category_id) selected @endif>{{$category->name_ar}}</option>
                                @endforeach
                            </select>
                            <label class="col-lg-10 form-control-label">يمكنك اختار قسم لمشاهده وتعديل الخصائص والقيم </label>
                        </div>
                    </div>
                    <div style="align-self: center;" class="form-group m-form__group col-md-1">
                        <button type="submit" class="btn btn-success">بحث</button>
                    </div>

                </div>
            </form>



            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th>#</th>
                    <th>اسم القسم بالعربيه</th>
                    <th>اسم القسم بالانجليزية</th>
                    <th>اسم الخاصيه بالعربيه</th>
                    <th>اسم الخاصيه بالانجليزيه</th>
                    <th>الايقونه</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($options as $index=>$option)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td>{{$option->category_ar}}</td>
                        <td>{{$option->category_en}}</td>
                        <td>{{$option->name_ar}}</td>
                        <td>{{$option->name_en}}</td>
                        <td>
                            @if($option->icon)
                            <img src="{{$option->icon}}" width="100px" height="100px">
                            @endif
                        </td>
                        <td>
                            <a title="تعديل" href="/webadmin/categories-options/{{$option->id}}/edit"><i
                                    class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/categories-options/{{ $option->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$options->links()}}
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/countrcountriess!!}--}}
@endsection
