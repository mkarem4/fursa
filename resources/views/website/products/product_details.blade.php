@extends("website.layouts.app")
@section('content')
    <div class="single-page">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="title">{{$product->title}}</h3>
                    <br>
                    <div class="owl-carousel owl-theme slider-big" dir="ltr">
                        @foreach($product->ProductImage as $product_image)
                            <div class="item">
                                <img src="{{$product_image->image}}">
                            </div>
                        @endforeach
                    </div>


                    <div class="imgs-single">
                        <div class="owl-carousel owl-theme slider2" dir="ltr">
                            @foreach($product->ProductImage as $product_image)
                                <div class="item">
                                    <img src="{{$product_image->image}}">
                                </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="info-short">
                        <ul>
                            <li><i class="fa fa-building"></i>{{$product->user->username}}</li>
                            <li>
                                <i class="fa fa-map-marker"></i>{{app()->isLocale('ar') ?$product->city->name_ar:$product->city->name_en}}
                            </li>
                            <li><i class="fa fa-clock-o"></i> {{$product->created_at->diffForHumans()}}</li>
                            <li><i class="fa fa-eye"></i>{{ $product->views }}</li>
                        </ul>
                    </div>

                    <div class="full-info">
                        @foreach($product->ProductOption as $option)
                            <p>@if($option->option->icon)
                                    <img src="{{$option->option->icon}}" width="25px"
                                         height="25px">@endif
                                {{app()->isLocale('ar') ?$option->option->name_ar:$option->option->name_en}}
                                : {{app()->isLocale('ar') ?($option->optionValue ? $option->optionValue->value : '------------'):($option->optionValue ? $option->optionValue->value_en :'---------')}}
                            </p>
                        @endforeach

                    </div>
                    <div class="clearfix"></div>
                    <div class="full-text">
                        <h4>{{trans('site.description')}}</h4>
                        <h5>{{ 	$product->description }}</h5>
                    </div>

                    {{--                    <div class="full-text">--}}
                    {{--                        <h4>المنطقه</h4>--}}

                    {{--                        <h5><i class="fa fa-map-marker"></i> Maple 1, Maple at Dubai Hills Estate, Dubai Hills Estate,--}}
                    {{--                            Dubai, UAE</h5>--}}

                    {{--                        <h5>--}}
                    {{--                            <iframe--}}
                    {{--                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d927755.0519564094!2d47.38302745861064!3d24.725398092904765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f03890d489399%3A0xba974d1c98e79fd5!2z2KfZhNix2YrYp9i2IDExNTY02Iwg2KfZhNiz2LnZiNiv2YrYqQ!5e0!3m2!1sar!2seg!4v1611614223052!5m2!1sar!2seg"--}}
                    {{--                                width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""--}}
                    {{--                                aria-hidden="false" tabindex="0"></iframe>--}}
                    {{--                        </h5>--}}
                    {{--                    </div>--}}

                </div>

                <div class="col-md-4">
                    @if($product->price)
                        <div class="price">{{ $product->price }} {{ Helpers::getCurrency() }}</div>
                    @endif
                    <div class="buy-option">
                        <a href="#" class="message-us">{{trans('site.chat')}}</a>
                        <a href="#" class="phone-show phone_show_text">{{trans('site.show_phone')}}</a>
                        <a href="tel:{{$product->phone}}" class="phone-show phone_show_number"
                           style="display: none"> {{$product->phone}}</a>
                    </div>

                    <div class="sent-me">
                        <h4><a href="/low_price/{{$product->id}}">
                                <i class="fa fa-info"></i>
                                {{trans('site.inform_low_price')}}
                            </a>
                        </h4>
                    </div>


                </div>

            </div>
        </div>
    </div>
@endsection

