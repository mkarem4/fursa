<section id="video-container" class="hide-in-sm">
    <div class="video-overlay"></div>
    <div class="video-content">
        <div class="inner">
            <span></span>
            <h2>{{trans('site.footer_title')}}</h2><br/>
            <div class="download">
                <a href="#"><img src="/website/img/android.png"></a>
                <a href="#"><img src="/website/img/apple.png"></a>
            </div>
        </div>
    </div>
    <video autoplay="" loop="" muted>
        <source src="highway-loop.mp4" type="/website/video/mp4"/>
    </video>
</section>
<footer class="hide-in-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="about-veno">
                    <div class="logo">
                        <img src="/website/img/rabtlogo.png" alt="Venue Logo" width="140px">
                    </div>
                    <ul class="social-icons">
                        <li>
                            <a href="{{$setting->facebook}}"><i class="fa fa-facebook"></i></a>
                            <a href="{{$setting->twitter}}"><i class="fa fa-twitter"></i></a>
                            <a href="{{$setting->instagram}}"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="useful-links">
                    <div class="footer-heading">
                        <h4>{{trans('site.company_info')}}</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li><a href="/about-us"><i class="fa fa-stop"></i>{{trans('site.about_us')}}</a></li>
                                <li><a href="/contact-us"><i class="fa fa-stop"></i>{{trans('site.contact_us')}}</a>
                                </li>
                                <li><a href="/adv"><i class="fa fa-stop"></i>{{trans('site.to_add')}}</a></li>
                                <li><a href="/packages"><i class="fa fa-stop"></i>{{trans('site.packages')}}</a></li>
                                <li><a href="/terms"><i class="fa fa-stop"></i>{{trans('site.terms')}}</a></li>
                                <li><a href="/privacy"><i class="fa fa-stop"></i>{{trans('site.privacy')}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="contact-info">
                    <div class="footer-heading">
                        <h4>{{trans('site.contact_info')}}</h4>
                    </div>
                    <ul>
                        <li><span>{{trans('site.phone')}} :</span><a
                                    href="tel:{{$setting->phone}}">{{$setting->phone}}</a>
                        </li>
                        <li><span>{{trans('site.email')}} :</span><a href="mailto:{{$setting->email}}">{{$setting->email}}</a>
                        </li>
                        <li><span>{{trans('site.address')}} :</span><a href="#">{{$setting->address}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>


<br>
<br>


<div class="fixed-footer">
    <ul>
        <li><a href="/"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href=""><i class="fa fa-search"></i>ابحث الان</a></li>
        <li><a href="/add-ad"><i class="fa fa-home"></i>اضف اعلان</a></li>
        <li><a href="/profile"><i class="fa fa-user"></i>حسابي</a></li>
    </ul>
</div>
