<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>فرصة - Forsa</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(app()->isLocale('en'))
    <link rel="stylesheet" href="{{asset('/website/css/bootstrapen.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/en.css')}}">
        <link rel="stylesheet" href="{{asset('/website/css/nav-en.css')}}">
        <style>
            #list-item-wrapper .item {
                margin: 10px 0;
                position: relative;
            }
            #list-item-wrapper .my4 {
                margin-top: 4px;
                margin-bottom: 4px;
            }
            #list-item-wrapper .block {
                clear: both;
                float: left;
                width: 100%;
            }
            #list-item-wrapper .item-title {
                margin-bottom: 8px;
                margin-top: 5px;
            }
            #list-item-wrapper .results-listing-title {
                color: #BC0000;
                float: left;
                font-size: 14px;
                font-weight: 700;
                margin: 0 0 4px;
                position: relative;
                width: 530px;
            }
            #list-item-wrapper .price {
                display: -ms-flexbox;
                -js-display: flex;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                font-size: 16px;
                float: right;
                color: #e00000;
                font-weight: 600;
                width: 170px;
                text-align: right;
                font-family: proxima-nova,GESS,helvetica,arial,sans-serif;
            }
            #list-item-wrapper .thumb {
                position: relative;
                float: left;
                width: 160px;
                margin-right: 20px;
                margin-bottom: 10px;
                clear: both;
            }
            #list-item-wrapper .thumb a {
                width: 168px;
                height: 114px;
                display: block;
            }
            #list-item-wrapper .thumb div {
                background-size: cover;
                background-position: 50%;
                width: 100%;
                height: 100%;
            }
            #list-item-wrapper .description {
                font-size: 12px;
                float: left;
                width: 565px;
            }
            #list-item-wrapper .breadcrumbs {
                clear: both;
                float: left;
                margin: 12px 0 12px 14px;
                font-weight: 700;
            }
            #list-item-wrapper .breadcrumbs {
                margin: 0;
                color: #3b4245;
                font-size: 14px;
                line-height: 1;
            }

            #list-item-wrapper .feature {
                list-style: none;
                margin: 0;
                padding: 0;
            }
            #list-item-wrapper .features {
                margin: 0 20px;
                padding: 0;
                list-style: none;
                float: left;
                font-size: 12px;
            }
            #list-item-wrapper .clear {
                clear: both;
                height: 0;
                line-height: 0;
            }
            .list-item-wrapper {
                border-bottom: 1px solid #d8d9da;
            }

        </style>

    @else
    <link rel="stylesheet" href="{{asset('/website/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('/website/css/nav.css')}}">
        <style>
            #list-item-wrapper .item {
                margin: 10px 0;
                position: relative;
            }
            #list-item-wrapper .my4 {
                margin-top: 4px;
                margin-bottom: 4px;
            }
            #list-item-wrapper .block {
                clear: both;
                float: right;
                width: 100%;
            }
            #list-item-wrapper .item-title {
                margin-bottom: 8px;
                margin-top: 5px;
            }
            #list-item-wrapper .results-listing-title {
                color: #BC0000;
                float: right;
                font-size: 14px;
                font-weight: 700;
                margin: 0 0 4px;
                position: relative;
                width: 530px;
                text-align: right;
            }
            #list-item-wrapper .price {
                display: -ms-flexbox;
                -js-display: flex;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                font-size: 16px;
                float: left;
                color: #e00000;
                font-weight: 600;
                width: 170px;
                text-align: left;
                font-family: proxima-nova,GESS,helvetica,arial,sans-serif;
            }
            #list-item-wrapper .thumb {
                position: relative;
                float: right;
                width: 160px;
                margin-right: 20px;
                margin-bottom: 10px;
                clear: both;
            }
            #list-item-wrapper .thumb a {
                width: 168px;
                height: 114px;
                display: block;
            }
            #list-item-wrapper .thumb div {
                background-size: cover;
                background-position: 50%;
                width: 100%;
                height: 100%;
            }
            #list-item-wrapper .description {
                font-size: 12px;
                float: left;
                width: 565px;
            }
            #list-item-wrapper .breadcrumbs {
                clear: both;
                float: right;
                margin: 12px 0 12px 14px;
                font-weight: 700;
            }
            #list-item-wrapper .breadcrumbs {
                margin: 0;
                color: #3b4245;
                font-size: 14px;
                line-height: 1;
            }

            #list-item-wrapper .feature {
                list-style: none;
                margin: 0;
                padding: 0;
            }
            #list-item-wrapper .features {
                margin: 0 20px;
                padding: 0;
                list-style: none;
                float: right;
                font-size: 12px;
            }
            #list-item-wrapper .clear {
                clear: both;
                height: 0;
                line-height: 0;
            }
            .list-item-wrapper {
                border-bottom: 1px solid #d8d9da;
            }
            .features{display: inline-flex;flex-direction: row;flex-wrap: wrap;justify-content: space-between;align-content: space-between}
            .features li {width: 33.3%;    margin-bottom: 5px}
            @if(app()->getLocale() == 'ar')
            .features li {text-align: right}
            .features li img {float: right}

            @endif
        </style>


    @endif
    <link rel="stylesheet" href="{{asset('/website/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/fontAwesome.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/hero-slider.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/owl-carousel.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/templatemo-style.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/custom.css')}}">
    <!-- link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Almarai&display=swap" rel="stylesheet">
    <script src="{{asset('/website/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>

    @stack('css')
</head>

<body>


@include('website.layouts.header')
@include('website.layouts.messages')


@yield('content')

@include('website.layouts.footer')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
<script>window.jQuery || document.write('<script src="{{asset('/website/js/vendor/jquery-1.11.2.min.js')}}"><\/script>')</script>

<script src="{{asset('/website/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('/website/js/datepicker.js')}}"></script>
<script src="{{asset('/website/js/plugins.js')}}"></script>
<script src="{{asset('/website/js/main.js')}}"></script>
<script src="{{asset('/admin/app/js/countries.js')}}"></script>


@stack('js')

</body>
</html>
