@extends("website.layouts.app")
@section('content')
    @push('css')
        <style>


            .avatar-upload {
                position: relative;
                max-width: 205px;
                margin: 50px auto;
            }

            .avatar-upload .avatar-edit {
                position: absolute;
                right: 12px;
                z-index: 1;
                top: 10px;
            }
            .avatar-upload .avatar-edit input {
                display: none;
            }
            .avatar-upload .avatar-edit input + label {
                display: inline-block;
                width: 34px;
                height: 34px;
                margin-bottom: 0;
                border-radius: 100%;
                background: #fff;
                border: 1px solid transparent;
                box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
                cursor: pointer;
                font-weight: normal;
                transition: all 0.2s ease-in-out;
            }
            .avatar-upload .avatar-edit input + label:hover {
                background: #f1f1f1;
                border-color: #d6d6d6;
            }
            .avatar-upload .avatar-edit input + label:after {
                font-family: 'FontAwesome';
                content: "\f030";

                color: #757575;
                position: absolute;
                top: 6px;
                left: 0;
                right: 0;
                text-align: center;
                margin: auto;
            }
            .avatar-upload .avatar-preview {
                width: 192px;
                height: 192px;
                position: relative;
                border-radius: 100%;
                border: 6px solid #f8f8f8;
                box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
            }
            .avatar-upload .avatar-preview > div {
                width: 100%;
                height: 100%;
                border-radius: 100%;
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
            }
            .sub-form { border:1px solid #eee; background-color: #eeeeee30}
            .basic-form {
                text-align: right;
                direction: rtl;
                background-color: #fff;
                padding: 10px

            }
            .helper-text  {text-align: center}
            .basic-form .form-control {text-align: right;}
            .text-decor {font-family: inherit;color: #337ab7;}
            .buttons { margin: auto 40%;}
            .buttons  .btn{    padding: 5px 20px; margin: auto 5px;}
            .multiselect  {padding: 0px 5px;}
            .avatar-upload2 {
                position: relative;
                max-width: inherit;
                margin: auto;
            }
            .avatar-upload2 .avatar-preview{
                border-radius: 0px;
                width: 100%;
                height: 150px;
            }
            .avatar-upload2 .avatar-preview > div{border-radius: 0px}

        </style>
    @endpush
    <div class="container">
        <div class="main-body">

            <!-- Breadcrumb -->
            <nav aria-label="breadcrumb" class="main-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">{{trans('site.home')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{trans('site.edit_profile')}}</li>
                </ol>
            </nav>
            <!-- /Breadcrumb -->

            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">

                    <form class="basic-form" id="basic-form" method="post" action="{{ url('profile') }}" enctype="multipart/form-data" autocomplete="off">
                        @csrf

                    <div class="card">
                        <div class="card-body">
                            <div class="your-image  col-md-12">
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' name="photo" id="imageUpload"  accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload"></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <div id="imagePreview"  style="background-image: url({{$user->photo?:'https://bootdey.com/img/Content/avatar/avatar7.png'}});">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-3">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.username')}}</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <input class="form-control" name="username" value="{{$user->username}}" type="text">

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.email')}}</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <input class="form-control" name="email" value="{{$user->email}}" type="text">

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.phone')}}</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <input class="form-control" name="phone" value="{{$user->phone}}" type="text">

                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.password')}}</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <input class="form-control" name="password"  placeholder="**********" value="" type="password">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.confirm_password')}}</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <input class="form-control" name="password_confirmation" placeholder="**********" value="" type="password">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.country')}}</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <select name="country_id" id="country_id" class="form-control" lang="{{app()->getLocale()}}" placeholder="{{trans('site.country')}}">
                                        <option value=""> {{trans('site.country')}}</option>
                                        @foreach($countries as $country)
                                            <option @if($user->country_id  == $country->id) selected @endif value="{{$country->id}}"> {{app()->isLocale('ar') ?$country->name_ar:$country->name_en}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">{{trans('site.city')}}</h5>

                                </div>
                                <div class="col-sm-7 text-secondary">
                                    <select name="city_id" id="city_id" class="form-control">
                                        @foreach($city as $item)
                                            <option @if($user->city_id  == $item->id) selected @endif value="{{$item->id}}"> {{app()->isLocale('ar') ?$item->name_ar:$item->name_en}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>





                            <div class="row">
                                <button type="submit" class="btn btn-block">تحديث</button>
                            </div>
                        </div>

                    </div>
                    </form>
                </div>
                <div class="col-md-8">
                    @if($products->isNotEmpty())
                    <section class="popular-places" id="popular">
                        <div class="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-heading">
                                        {{--<span>اكسبريس ديليفري سيرفس</span>--}}
                                        <h2>{{trans('site.my_ads')}}</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="" dir="ltr">
                                @foreach($products as $product)
                                <div class="col-md-4 col-xs-6 popular-item">
                                    <div class="thumb">
                                        <a href="/product/{{$product->id}}"><img src="{{count($product->ProductImage) > 0?$product->ProductImage[0]->image:''}}" alt=""></a>
                                        <div class="text-content">
                                            <h4>{{$product->title}}</h4>
                                            <span>{{$product->price}} {{ Helpers::getCurrency() }}</span>
                                        </div>

                                    </div>
                                </div>
                                @endforeach


                            </div>
                        </div>
                    </section>
                    @endif

                </div>
            </div>
        </div>
    </div>


    @push('js')
        <script>

            function readURL1(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                        $('#imagePreview').hide();
                        $('#imagePreview').fadeIn(650);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#imageUpload").change(function() {
                readURL1(this);
            });
        </script>
        <script src="{{asset('control/plugins/select2/js/select2.full.min.js')}}"></script>
        <script>


            $(function () {
                $('#roles').select2({
                    'placeholder':'قم باختيار الصلاحيه المناسبه'
                });
            })


        </script>
    @endpush
@endsection