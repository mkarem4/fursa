@extends("website.layouts.app")
@section('content')
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        {{--<span>اكسبريس ديليفري سيرفس</span>--}}
                        <h2>{{trans('site.favs')}}</h2>
                    </div>
                </div>
            </div>
            @if($products->isNotEmpty())
            <div class="" dir="ltr">
                @foreach($products as $product)
                <div class="col-md-3 col-xs-6 popular-item">
                    <div class="thumb">
                        <a href="/product/{{$product->id}}"><img src="{{$product->ProductImage[0]->image}}" alt=""></a>
                        <div class="text-content">
                            <h4>{{$product->title}}</h4>
                            <span>{{$product->price}} {{ Helpers::getCurrency() }}</span>
                        </div>

                    </div>
                </div>
                @endforeach


            </div>
            @else
                <span class="text-danger text-bold">{{trans('site.empty_fav')}}</span>
            @endif
        </div>
    </section>
@endsection