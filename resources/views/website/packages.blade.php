@extends("website.layouts.app")
@section('content')


    <style>
        * {
            margin: 0;
            padding: 0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        ul {
            list-style-type: none;
        }

        .pricing-table-title {
            text-transform: uppercase;
            font-weight: 700;
            font-size: 2.6em;
            color: #FFF;
            margin-top: 15px;
            text-align: left;
            margin-bottom: 25px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
        }

        .pricing-table-title a {
            font-size: 0.6em;
        }

        .clearfix:after {
            content: '';
            display: block;
            height: 0;
            width: 0;
            clear: both;
        }

        /** ========================
         * Contenedor
         ============================*/
        .pricing-wrapper {
            width: 960px;
            margin: 40px auto 0;
        }

        .pricing-table {
            margin: 0 10px;
            text-align: center;
            width: 300px;
            float: left;
            -webkit-box-shadow: 0 0 15px rgba(0, 0, 0, 0.4);
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.4);
            -webkit-transition: all 0.25s ease;
            -o-transition: all 0.25s ease;
            transition: all 0.25s ease;
        }

        .pricing-table:hover {
            -webkit-transform: scale(1.06);
            -ms-transform: scale(1.06);
            -o-transform: scale(1.06);
            transform: scale(1.06);
        }

        .pricing-title {
            color: #FFF;
            background: #ee7723;
            padding: 20px 0;
            font-size: 2em;
            text-transform: uppercase;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
        }

        .pricing-table.recommended .pricing-title {
            background: #2db3cb !important;
        }

        .pricing-table.recommended .pricing-action {
            background: #2db3cb !important;
        }

        .pricing-table.gold .pricing-title {
            background: gold !important;
        }

        .pricing-table.recommended .pricing-action {
            background: gold !important;
        }

        .pricing-table .price {
            background: #403e3d;
            font-size: 3.4em;
            font-weight: 700;
            padding: 20px 0;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
        }

        .pricing-table .price sup {
            font-size: 0.4em;
            position: relative;
            left: 5px;
        }

        .table-list {
            background: #FFF;
            color: #403d3a;
        }

        .table-list li {
            font-size: 1.4em;
            font-weight: 700;
            padding: 12px 8px;
        }

        .table-list li:before {
            content: "\f00c";
            font-family: 'FontAwesome';
            color: #3fab91;
            display: inline-block;
            position: relative;
            right: 5px;
            font-size: 16px;
        }

        .table-list li span {
            font-weight: 400;
        }

        .table-list li span.unlimited {
            color: #FFF;
            background: #ee7723;
            font-size: 0.9em;
            padding: 5px 7px;
            display: inline-block;
            -webkit-border-radius: 38px;
            -moz-border-radius: 38px;
            border-radius: 38px;
        }


        .table-list li:nth-child(2n) {
            background: #F0F0F0;
        }

        .table-buy {
            background: #FFF;
            padding: 15px;
            text-align: left;
            overflow: hidden;
        }

        .table-buy p {
            float: left;
            color: #37353a;
            font-weight: 700;
            font-size: 2.4em;
        }

        .table-buy p sup {
            font-size: 0.5em;
            position: relative;
            left: 5px;
        }

        .table-buy .pricing-action {
            float: right;
            color: #FFF;
            background: #ee7723;
            padding: 10px 16px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            font-weight: 700;
            font-size: 1.4em;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
            -webkit-transition: all 0.25s ease;
            -o-transition: all 0.25s ease;
            transition: all 0.25s ease;
        }

        .table-buy .pricing-action:hover {
            background: #cf4f3e;
        }

        .recommended .table-buy .pricing-action:hover {
            background: #228799;
        }

        /** ================
         * Responsive
         ===================*/
        @media only screen and (min-width: 768px) and (max-width: 959px) {
            .pricing-wrapper {
                width: 768px;
            }

            .pricing-table {
                width: 236px;
            }

            .table-list li {
                font-size: 1.3em;
            }

        }

        @media only screen and (max-width: 767px) {
            .pricing-wrapper {
                width: 420px;
            }

            .pricing-table {
                display: block;
                float: none;
                margin: 0 0 20px 0;
                width: 100%;
            }
        }

        @media only screen and (max-width: 479px) {
            .pricing-wrapper {
                width: 300px;
            }
        }
    </style>
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span style="margin:0 149px">{{trans('site.nav_title')}}</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item" style="border-right:none">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="{{trans('site.search')}}">
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit"
                                                            class="btn">{{trans('site.search')}}</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="popular-places" id="popular">
        <div class="container">

            <!-- Contenedor -->
            <div class="pricing-wrapper clearfix">
                <!-- Titulo -->


                <div class="pricing-table gold">
                    <h3 class="pricing-title gold">{{trans('site.golden_membership')}}</h3>
                    <!-- Lista de Caracteristicas / Propiedades -->
                    <ul class="table-list">
                        <li>{{trans('site.free_ad')}}</li>
                        <li> 15 <span> {{trans('site.special_ads')}} </span></li>
                        <li> 5 <span> ا{{trans('site.paid_ads')}} </span></li>
                        <li> 1 <span> {{trans('site.one_ad_for_week')}} </span></li>
                        <li><span>{{trans('site.cost_for_month')}}</span> 50$</li>
                        <li><span>{{trans('site.cost_for_6_months')}}</span> 250$</li>
                        <li><span>{{trans('site.cost_for_year')}}</span> 350$</li>
                    </ul>
                </div>

                <div class="pricing-table recommended">
                    <h3 class="pricing-title recommended">{{trans('site.special_membership')}}</h3>
                    <!-- Lista de Caracteristicas / Propiedades -->
                    <ul class="table-list">
                        <li>{{trans('site.free_ad')}}</li>
                        <li> 10 <span> {{trans('site.special_ads')}} </span></li>
                        <li> {{trans('site.no')}} <span>{{trans('site.paid_ads')}} </span></li>
                        <li> {{trans('site.no')}} <span> {{trans('site.one_ad_for_week')}} </span></li>
                        <li><span>{{trans('site.cost_for_month')}}</span> 10$</li>
                        <li><span>{{trans('site.cost_for_6_months')}}</span> 50$</li>
                        <li><span>{{trans('site.cost_for_year')}}</span> 70$</li>
                    </ul>
                </div>

                <div class="pricing-table">
                    <h3 class="pricing-title">{{trans('site.normal_membership')}}</h3>
                    <!-- Lista de Caracteristicas / Propiedades -->
                    <ul class="table-list">
                        <li> {{trans('site.free_ad')}}</li>
                        <li> {{trans('site.no')}} <span> {{trans('site.special_ads')}} </span></li>
                        <li> {{trans('site.no')}} <span> {{trans('site.paid_ads')}} </span></li>
                        <li> {{trans('site.no')}} <span> {{trans('site.one_ad_for_week')}} </span></li>
                        <li><span>{{trans('site.cost_for_month')}}</span> {{trans('site.free')}}</li>
                        <li><span>{{trans('site.cost_for_6_months')}}</span> {{trans('site.free')}}</li>
                        <li><span>{{trans('site.cost_for_year')}}</span> {{trans('site.free')}}</li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
@endsection
